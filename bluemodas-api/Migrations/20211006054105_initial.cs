﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bluemodas_api.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Carrinhos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Finalizado = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Carrinhos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Produtos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(type: "TEXT", nullable: true),
                    Valor = table.Column<double>(type: "REAL", nullable: false),
                    Descricao = table.Column<string>(type: "TEXT", nullable: true),
                    Imagem = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produtos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Registros",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Nome = table.Column<string>(type: "TEXT", nullable: true),
                    Email = table.Column<string>(type: "TEXT", nullable: true),
                    Telefone = table.Column<string>(type: "TEXT", nullable: true),
                    Senha = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Registros", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Pedidos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RegistroId = table.Column<int>(type: "INTEGER", nullable: true),
                    Observacao = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pedidos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pedidos_Registros_RegistroId",
                        column: x => x.RegistroId,
                        principalTable: "Registros",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PedidosProdutos",
                columns: table => new
                {
                    ProdutoId = table.Column<int>(type: "INTEGER", nullable: false),
                    PedidoId = table.Column<int>(type: "INTEGER", nullable: false),
                    Quantidade = table.Column<int>(type: "INTEGER", nullable: false),
                    Valor = table.Column<double>(type: "REAL", nullable: false),
                    CarrinhoId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PedidosProdutos", x => new { x.ProdutoId, x.PedidoId });
                    table.ForeignKey(
                        name: "FK_PedidosProdutos_Carrinhos_CarrinhoId",
                        column: x => x.CarrinhoId,
                        principalTable: "Carrinhos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PedidosProdutos_Pedidos_PedidoId",
                        column: x => x.PedidoId,
                        principalTable: "Pedidos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PedidosProdutos_Produtos_ProdutoId",
                        column: x => x.ProdutoId,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Carrinhos",
                columns: new[] { "Id", "Finalizado" },
                values: new object[] { 1, false });

            migrationBuilder.InsertData(
                table: "Carrinhos",
                columns: new[] { "Id", "Finalizado" },
                values: new object[] { 2, false });

            migrationBuilder.InsertData(
                table: "Carrinhos",
                columns: new[] { "Id", "Finalizado" },
                values: new object[] { 3, false });

            migrationBuilder.InsertData(
                table: "Carrinhos",
                columns: new[] { "Id", "Finalizado" },
                values: new object[] { 4, false });

            migrationBuilder.InsertData(
                table: "Carrinhos",
                columns: new[] { "Id", "Finalizado" },
                values: new object[] { 5, false });

            migrationBuilder.InsertData(
                table: "Produtos",
                columns: new[] { "Id", "Descricao", "Imagem", "Nome", "Valor" },
                values: new object[] { 1, "Lindos vestidos", "https://source.unsplash.com/1600x800/?dress", "Vestido", 100.3 });

            migrationBuilder.InsertData(
                table: "Produtos",
                columns: new[] { "Id", "Descricao", "Imagem", "Nome", "Valor" },
                values: new object[] { 2, "Lindas blusas", "https://source.unsplash.com/1600x800/?t-shirts", "Blusa", 35.689999999999998 });

            migrationBuilder.InsertData(
                table: "Produtos",
                columns: new[] { "Id", "Descricao", "Imagem", "Nome", "Valor" },
                values: new object[] { 3, "Lindas calças", "https://source.unsplash.com/1600x800/?pants", "Calça", 124.12 });

            migrationBuilder.InsertData(
                table: "Produtos",
                columns: new[] { "Id", "Descricao", "Imagem", "Nome", "Valor" },
                values: new object[] { 4, "Lindos relógios", "https://source.unsplash.com/1600x800/?rolex", "Relógio", 299.99000000000001 });

            migrationBuilder.InsertData(
                table: "Produtos",
                columns: new[] { "Id", "Descricao", "Imagem", "Nome", "Valor" },
                values: new object[] { 5, "Lindos tênis", "https://source.unsplash.com/1600x800/?shoes", "Tênis", 70.989999999999995 });

            migrationBuilder.InsertData(
                table: "Produtos",
                columns: new[] { "Id", "Descricao", "Imagem", "Nome", "Valor" },
                values: new object[] { 6, "Lindas saias", "https://source.unsplash.com/1600x800/?skirts", "Saia", 50.0 });

            migrationBuilder.InsertData(
                table: "Registros",
                columns: new[] { "Id", "Email", "Nome", "Senha", "Telefone" },
                values: new object[] { 1, "marta@gmail.com", "Marta", "senha12", "33225555" });

            migrationBuilder.InsertData(
                table: "Registros",
                columns: new[] { "Id", "Email", "Nome", "Senha", "Telefone" },
                values: new object[] { 2, "paula@gmail.com", "Paula", "senha34", "3354288" });

            migrationBuilder.InsertData(
                table: "Registros",
                columns: new[] { "Id", "Email", "Nome", "Senha", "Telefone" },
                values: new object[] { 3, "laura@gmail.com", "Laura", "senha56", "55668899" });

            migrationBuilder.InsertData(
                table: "Registros",
                columns: new[] { "Id", "Email", "Nome", "Senha", "Telefone" },
                values: new object[] { 4, "luiza@gmail.com", "Luiza", "senha78", "6565659" });

            migrationBuilder.InsertData(
                table: "Pedidos",
                columns: new[] { "Id", "Observacao", "RegistroId" },
                values: new object[] { 4, "Observação 4", 1 });

            migrationBuilder.InsertData(
                table: "Pedidos",
                columns: new[] { "Id", "Observacao", "RegistroId" },
                values: new object[] { 1, "Observação 1", 2 });

            migrationBuilder.InsertData(
                table: "Pedidos",
                columns: new[] { "Id", "Observacao", "RegistroId" },
                values: new object[] { 2, "Observação 2", 3 });

            migrationBuilder.InsertData(
                table: "Pedidos",
                columns: new[] { "Id", "Observacao", "RegistroId" },
                values: new object[] { 5, "Observação 5", 3 });

            migrationBuilder.InsertData(
                table: "Pedidos",
                columns: new[] { "Id", "Observacao", "RegistroId" },
                values: new object[] { 3, "Observação 3", 4 });

            migrationBuilder.InsertData(
                table: "PedidosProdutos",
                columns: new[] { "PedidoId", "ProdutoId", "CarrinhoId", "Quantidade", "Valor" },
                values: new object[] { 4, 6, null, 2, 100.0 });

            migrationBuilder.InsertData(
                table: "PedidosProdutos",
                columns: new[] { "PedidoId", "ProdutoId", "CarrinhoId", "Quantidade", "Valor" },
                values: new object[] { 1, 1, null, 2, 200.59999999999999 });

            migrationBuilder.InsertData(
                table: "PedidosProdutos",
                columns: new[] { "PedidoId", "ProdutoId", "CarrinhoId", "Quantidade", "Valor" },
                values: new object[] { 1, 2, null, 4, 142.75999999999999 });

            migrationBuilder.InsertData(
                table: "PedidosProdutos",
                columns: new[] { "PedidoId", "ProdutoId", "CarrinhoId", "Quantidade", "Valor" },
                values: new object[] { 2, 3, null, 6, 744.72000000000003 });

            migrationBuilder.InsertData(
                table: "PedidosProdutos",
                columns: new[] { "PedidoId", "ProdutoId", "CarrinhoId", "Quantidade", "Valor" },
                values: new object[] { 2, 4, null, 3, 899.97000000000003 });

            migrationBuilder.InsertData(
                table: "PedidosProdutos",
                columns: new[] { "PedidoId", "ProdutoId", "CarrinhoId", "Quantidade", "Valor" },
                values: new object[] { 5, 1, null, 5, 501.5 });

            migrationBuilder.InsertData(
                table: "PedidosProdutos",
                columns: new[] { "PedidoId", "ProdutoId", "CarrinhoId", "Quantidade", "Valor" },
                values: new object[] { 3, 5, null, 1, 354.94999999999999 });

            migrationBuilder.CreateIndex(
                name: "IX_Pedidos_RegistroId",
                table: "Pedidos",
                column: "RegistroId");

            migrationBuilder.CreateIndex(
                name: "IX_PedidosProdutos_CarrinhoId",
                table: "PedidosProdutos",
                column: "CarrinhoId");

            migrationBuilder.CreateIndex(
                name: "IX_PedidosProdutos_PedidoId",
                table: "PedidosProdutos",
                column: "PedidoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PedidosProdutos");

            migrationBuilder.DropTable(
                name: "Carrinhos");

            migrationBuilder.DropTable(
                name: "Pedidos");

            migrationBuilder.DropTable(
                name: "Produtos");

            migrationBuilder.DropTable(
                name: "Registros");
        }
    }
}
