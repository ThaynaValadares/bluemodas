using System;
using System.Threading.Tasks;
using System.Collections.Generic;  
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using bluemodas_api.Data;
using bluemodas_api.Models;
namespace bluemodas_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class PedidoProdutoController : ControllerBase
    {
        private readonly IRepository repositorio;

        public PedidoProdutoController(IRepository repositorio)
        {
            this.repositorio = repositorio;
        }

        [HttpPost]
        public async Task<IActionResult> Post(List<PedidoProduto> model)
        {
            bool resultado = false;
            foreach(PedidoProduto item in (List<PedidoProduto>)model)
            {
                try {
                    this.repositorio.Add(item);

                    if(await this.repositorio.SaveChangesAsync()) {
                        resultado = true;
                    } else  {
                        resultado = false;
                    }
                }
                catch (Exception ex)
                {
                    resultado = false;
                }
            }

            if(resultado) return Ok(1);
            return BadRequest();
        }
    }
}