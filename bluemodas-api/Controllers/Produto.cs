using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using bluemodas_api.Data;
using bluemodas_api.Models;


namespace bluemodas_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class ProdutoController : ControllerBase
    {
        private readonly IRepository repositorio;

        public ProdutoController(IRepository repositorio)
        {
            this.repositorio = repositorio;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try {
                var resultado = await this.repositorio.GetAllProdutosAsync(false);

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpGet("{ProdutoId}")]
        public async Task<IActionResult> GetByProdutoId(int ProdutoId)
        {
            try {
                var resultado = await this.repositorio.GetProdutoAsyncById(ProdutoId, true);

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(Produto model)
        {
            try {
                this.repositorio.Add(model);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(model);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpPut("{ProdutoId}")]
        public async Task<IActionResult> Put(int ProdutoId, Produto model)
        {
            try {

                var produto = await  this.repositorio.GetProdutoAsyncById(ProdutoId, false);
                
                if(produto == null) return NotFound();

                this.repositorio.Update(model);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(model);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpDelete("{ProdutoId}")]
        public async Task<IActionResult> Delete(int ProdutoId)
        {
            try {

                var produto = await  this.repositorio.GetProdutoAsyncById(ProdutoId, false);
                
                if(produto == null) return NotFound();

                this.repositorio.Delete(produto);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(1);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }
    }
}