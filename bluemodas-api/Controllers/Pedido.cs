using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using bluemodas_api.Data;
using bluemodas_api.Models;

namespace bluemodas_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class PedidoController : ControllerBase
    {
        private readonly IRepository repositorio;

        public PedidoController(IRepository repositorio)
        {
            this.repositorio = repositorio;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try {
                var resultado = await this.repositorio.GetAllPedidosAsync(true);

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpGet("{PedidoId}")]
        public async Task<IActionResult> GetByPedidoId(int PedidoId)
        {
            try {
                var resultado = await this.repositorio.GetPedidoAsyncById(PedidoId, true);

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpGet("Registro/{RegistroId}")]
        public async Task<IActionResult> GetByPedidoRegistroId(int RegistroId)
        {
            try {
                var resultado = await this.repositorio.GetPedidoAsyncByRegistroId(RegistroId, true);

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }



        [HttpGet("Last")]
        public async Task<IActionResult> GetByPedidoLast()
        {
            try {
                var resultado = await this.repositorio.GetPedidoAsyncLast();

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(Pedido model)
        {
            try {
                this.repositorio.Add(model);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(model.Id);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpPut("{PedidoId}")]
        public async Task<IActionResult> Put(int PedidoId, Pedido model)
        {
            try {

                var pedido = await  this.repositorio.GetPedidoAsyncById(PedidoId, false);
                
                if(pedido == null) return NotFound();

                this.repositorio.Update(model);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(model);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpDelete("{PedidoId}")]
        public async Task<IActionResult> Delete(int PedidoId)
        {
            try {

                var pedido = await  this.repositorio.GetPedidoAsyncById(PedidoId, false);
                
                if(pedido == null) return NotFound();

                this.repositorio.Delete(pedido);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(1);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }
    }
}