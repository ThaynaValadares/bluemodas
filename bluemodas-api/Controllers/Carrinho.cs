using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using bluemodas_api.Data;
using bluemodas_api.Models;


namespace bluemodas_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class CarrinhoController : ControllerBase
    {
        private readonly IRepository repositorio;

        public CarrinhoController(IRepository repositorio)
        {
            this.repositorio = repositorio;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try {
                var resultado = await this.repositorio.GetAllCarrinhosAsync(false);

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpGet("{CarrinhoId}")]
        public async Task<IActionResult> GetByCarrinhoId(int CarrinhoId)
        {
            try {
                var resultado = await this.repositorio.GetCarrinhoAsyncById(CarrinhoId, true);

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(Carrinho model)
        {
            try {
                this.repositorio.Add(model);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(model.Id);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpPut("{CarrinhoId}")]
        public async Task<IActionResult> Put(int CarrinhoId, Carrinho model)
        {
            try {

                var carrinho = await  this.repositorio.GetCarrinhoAsyncById(CarrinhoId, false);
                
                if(carrinho == null) return NotFound();

                this.repositorio.Update(model);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(model);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpDelete("{CarrinhoId}")]
        public async Task<IActionResult> Delete(int CarrinhoId)
        {
            try {

                var carrinho = await  this.repositorio.GetCarrinhoAsyncById(CarrinhoId, false);
                
                if(carrinho == null) return NotFound();

                this.repositorio.Delete(carrinho);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(1);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }
    }
}