using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using bluemodas_api.Data;
using bluemodas_api.Models;

namespace bluemodas_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class RegistroController : ControllerBase
    {
        private readonly IRepository repositorio;

        public RegistroController(IRepository repositorio)
        {
            this.repositorio = repositorio;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try {
                var resultado = await this.repositorio.GetAllRegistrosAsync(true);

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpGet("{RegistroId}")]
        public async Task<IActionResult> GetByRegistroId(int RegistroId)
        {
            try {
                var resultado = await this.repositorio.GetRegistroAsyncById(RegistroId, false);

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpGet("login/{Email}/{Senha}")]
        public async Task<IActionResult> GetByRegistroEmailSenha(String Email, String Senha)
        {
            try {
                var resultado = await this.repositorio.GetRegistroAsyncByEmailSenha(Email, Senha);

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(Registro model)
        {
            try {
                this.repositorio.Add(model);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(model.Id);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpPut("{RegistroId}")]
        public async Task<IActionResult> Put(int RegistroId, Registro model)
        {
            try {

                var registro = await  this.repositorio.GetRegistroAsyncById(RegistroId, false);
                
                if(registro == null) return NotFound();

                this.repositorio.Update(model);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(model);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }

        [HttpDelete("{RegistroId}")]
        public async Task<IActionResult> Delete(int RegistroId)
        {
            try {

                var registro = await  this.repositorio.GetRegistroAsyncById(RegistroId, false);
                
                if(registro == null) return NotFound();

                this.repositorio.Delete(registro);

                if(await this.repositorio.SaveChangesAsync()) {
                    return Ok(1);
                } else  {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Erro: {ex.Message}");
            }
        }
    }
}