using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using bluemodas_api.Models;

namespace bluemodas_api.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base (options) { }   
        public DbSet<Carrinho> Carrinhos { get; set; }     
        public DbSet<Pedido> Pedidos { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Registro> Registros { get; set; }
        public DbSet<PedidoProduto> PedidosProdutos { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            
            builder.Entity<PedidoProduto>()
                .HasKey(PP => new { PP.ProdutoId, PP.PedidoId});

            builder.Entity<Carrinho>()
                .HasData(new List<Carrinho>(){
                    new Carrinho(1, false),
                    new Carrinho(2, false),
                    new Carrinho(3, false),
                    new Carrinho(4, false),
                    new Carrinho(5, false)
                });

            builder.Entity<Pedido>()
                .HasData(new List<Pedido>(){
                    new Pedido(1, 2, "Observação 1"),
                    new Pedido(2, 3, "Observação 2"),
                    new Pedido(3, 4, "Observação 3"),
                    new Pedido(4, 1, "Observação 4"),
                    new Pedido(5, 3, "Observação 5")
                });
            
            builder.Entity<Produto>()
                .HasData(new List<Produto>{
                    new Produto(1, "Vestido", "Lindos vestidos", 100.3, "https://source.unsplash.com/1600x800/?dress"),
                    new Produto(2, "Blusa", "Lindas blusas", 35.69, "https://source.unsplash.com/1600x800/?t-shirts"),
                    new Produto(3, "Calça", "Lindas calças", 124.12, "https://source.unsplash.com/1600x800/?pants"),
                    new Produto(4, "Relógio", "Lindos relógios", 299.99, "https://source.unsplash.com/1600x800/?rolex"),
                    new Produto(5, "Tênis", "Lindos tênis", 70.99, "https://source.unsplash.com/1600x800/?shoes"),
                    new Produto(6, "Saia", "Lindas saias", 50, "https://source.unsplash.com/1600x800/?skirts")
                });
            
            builder.Entity<Registro>()
                .HasData(new List<Registro>(){
                    new Registro(1, "Marta", "marta@gmail.com", "33225555", "senha12"),
                    new Registro(2, "Paula", "paula@gmail.com", "3354288", "senha34"),
                    new Registro(3, "Laura", "laura@gmail.com", "55668899", "senha56"),
                    new Registro(4, "Luiza", "luiza@gmail.com", "6565659", "senha78")
                });

            builder.Entity<PedidoProduto>()
                .HasData(new List<PedidoProduto>() {
                    new PedidoProduto() {ProdutoId = 1, PedidoId = 1, Quantidade = 2, Valor = 200.6},
                    new PedidoProduto() {ProdutoId = 2, PedidoId = 1, Quantidade = 4, Valor = 142.76},
                    new PedidoProduto() {ProdutoId = 3, PedidoId = 2, Quantidade = 6, Valor = 744.72},
                    new PedidoProduto() {ProdutoId = 4, PedidoId = 2, Quantidade = 3, Valor = 899.97},
                    new PedidoProduto() {ProdutoId = 5, PedidoId = 3, Quantidade = 1, Valor = 354.95},
                    new PedidoProduto() {ProdutoId = 6, PedidoId = 4, Quantidade = 2, Valor = 100},
                    new PedidoProduto() {ProdutoId = 1, PedidoId = 5, Quantidade = 5, Valor = 501.5}
                });
        }
        
    }
}