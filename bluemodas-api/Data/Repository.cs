using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.EntityFrameworkCore;
using bluemodas_api.Models;

namespace bluemodas_api.Data
{
    public class Repository : IRepository
    {
        private readonly DataContext _context;

        public Repository(DataContext context)
        {
            _context = context;
        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }
        public void Update<T>(T entity) where T : class
        {
            _context.Update(entity);
        }
        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }
        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }

        public async Task<Pedido[]> GetAllPedidosAsync(bool includeProduto = false)
        {
            IQueryable<Pedido> query = _context.Pedidos;

            if (includeProduto)
            {
                query = query.Include(pp => pp.PedidosProdutos);
            }

            query = query.AsNoTracking()
                         .OrderBy(c => c.Id);

            return await query.ToArrayAsync();
        }
        public async Task<Pedido> GetPedidoAsyncById(int pedidoId, bool includeProduto)
        {
            IQueryable<Pedido> query = _context.Pedidos;

            if (includeProduto)
            {
                query = query.Include(pp => pp.PedidosProdutos);
            }

            query = query.AsNoTracking()
                         .OrderBy(pedido => pedido.Id)
                         .Where(pedido => pedido.Id == pedidoId);

            return await query.FirstOrDefaultAsync();
        }
        public async Task<Pedido> GetPedidoAsyncByRegistroId(int registroId, bool includeProduto)
        {
            IQueryable<Pedido> query = _context.Pedidos;

            if (includeProduto)
            {
                query = query.Include(pp => pp.PedidosProdutos);
            }

            query = query.AsNoTracking()
                         .OrderBy(pedido => pedido.Id)
                         .Where(pedido => pedido.RegistroId == registroId);

            return await query.FirstOrDefaultAsync();
        }
        public async Task<Pedido> GetPedidoAsyncLast()
        {
            IQueryable<Pedido> query = _context.Pedidos;

            query = query.AsNoTracking()
                         .OrderByDescending(p => p.Id);

            return await query.FirstOrDefaultAsync();
        }
        public async Task<Produto[]> GetAllProdutosAsync(bool includePedido = false)
        {
            IQueryable<Produto> query = _context.Produtos;

            if (includePedido)
            {
                query = query.Include(pp => pp.PedidosProdutos);
            }

            query = query.AsNoTracking()
                         .OrderBy(c => c.Id);

            return await query.ToArrayAsync();
        }
        public async Task<Produto> GetProdutoAsyncById(int produtoId, bool includePedido)
        {
            IQueryable<Produto> query = _context.Produtos;

            if (includePedido)
            {
                query = query.Include(pp => pp.PedidosProdutos);
            }

            query = query.AsNoTracking()
                         .OrderBy(produto => produto.Id)
                         .Where(produto => produto.Id == produtoId);

            return await query.FirstOrDefaultAsync();
        }
        public async Task<Registro[]> GetAllRegistrosAsync(bool includePedido = false)
        {
            IQueryable<Registro> query = _context.Registros;

            if (includePedido)
            {
                query = query.Include(pp => pp.Pedidos);
            }

            query = query.AsNoTracking()
                         .OrderBy(c => c.Id);

            return await query.ToArrayAsync();
        }
        public async Task<Registro> GetRegistroAsyncById(int registroId, bool includePedido)
        {
            IQueryable<Registro> query = _context.Registros;

            if (includePedido)
            {
                query = query.Include(pp => pp.Pedidos);
            }

            query = query.AsNoTracking()
                         .OrderBy(registro => registro.Id)
                         .Where(registro => registro.Id == registroId);

            return await query.FirstOrDefaultAsync();
        }
        public async Task<Registro> GetRegistroAsyncByEmailSenha(string email, string senha)
        {
            IQueryable<Registro> query = _context.Registros;

            query = query.AsNoTracking()
                         .OrderBy(registro => registro.Id)
                         .Where(registro => registro.Email == email)
                         .Where(registro => registro.Senha == senha);

            return await query.FirstOrDefaultAsync();
        }
        public async Task<Carrinho[]> GetAllCarrinhosAsync(bool includeProduto = false)
        {
            IQueryable<Carrinho> query = _context.Carrinhos;

            if (includeProduto)
            {
                query = query.Include(pp => pp.PedidosProdutos);
            }

            query = query.AsNoTracking()
                         .OrderBy(c => c.Id);

            return await query.ToArrayAsync();
        }
        public async Task<Carrinho> GetCarrinhoAsyncById(int carrinhoId, bool includeProduto)
        {
            IQueryable<Carrinho> query = _context.Carrinhos;

            if (includeProduto)
            {
                query = query.Include(pp => pp.PedidosProdutos);
            }

            query = query.AsNoTracking()
                         .OrderBy(carrinho => carrinho.Id)
                         .Where(carrinho => carrinho.Id == carrinhoId);

            return await query.FirstOrDefaultAsync();
        }
    }
}