using System.Threading.Tasks;
using System.Text;
using bluemodas_api.Models;

namespace bluemodas_api.Data
{
    public interface IRepository
    {
        void Add<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveChangesAsync();

        Task<Pedido[]> GetAllPedidosAsync(bool includeProduto);        
        Task<Pedido> GetPedidoAsyncById(int pedidoId, bool includeProduto);
        Task<Pedido> GetPedidoAsyncByRegistroId(int registroId, bool includeProduto);
        Task<Pedido> GetPedidoAsyncLast();
        Task<Produto[]> GetAllProdutosAsync(bool includePedido);        
        Task<Produto> GetProdutoAsyncById(int produtoId, bool includePedido);
        Task<Registro[]> GetAllRegistrosAsync(bool includePedido);        
        Task<Registro> GetRegistroAsyncById(int registroId, bool includePedido);
        Task<Registro> GetRegistroAsyncByEmailSenha(string email, string senha);
        Task<Carrinho[]> GetAllCarrinhosAsync(bool includeProduto);        
        Task<Carrinho> GetCarrinhoAsyncById(int carrinhoId, bool includeProduto);
    }
}