using System.Collections.Generic;

namespace bluemodas_api.Models
{
    public class Produto
    {
        public Produto() {}

        public Produto(int id, string nome, string descricao, double valor,  string imagem)
        {
            this.Id = id;
            this.Nome = nome;
            this.Valor = valor;
            this.Descricao = descricao;
            this.Imagem = imagem;
        }
        
        public int Id { get; set; }
        public string Nome { get; set; }
        public double Valor { get; set; }
        public string Descricao { get; set; }
        public string Imagem { get; set; }
        public IEnumerable<PedidoProduto> PedidosProdutos { get; set; }
    }
}