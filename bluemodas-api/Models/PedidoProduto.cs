using System.Collections.Generic;  

namespace bluemodas_api.Models
{
    public class PedidoProduto
    {
        public PedidoProduto() { }
        
        public PedidoProduto(int produtoId, int pedidoId, int quantidade, double valor)
        {
            this.ProdutoId = produtoId;
            this.PedidoId = pedidoId;
            this.Quantidade = quantidade;
            this.Valor = valor;
        }
        public int ProdutoId { get; set; }
        public Produto Produto { get; set; }
        public int PedidoId { get; set; }
        public Pedido Pedido { get; set; }
        public int Quantidade { get; set; }
        public double Valor { get; set; }
    }
}