using System.Collections.Generic;

namespace bluemodas_api.Models
{
    public class Registro
    {
        public Registro() {}

        public Registro(int id, string nome, string email, string telefone, string senha)
        {
            this.Id = id;
            this.Nome = nome;
            this.Email = email;
            this.Telefone = telefone;
            this.Senha = senha;
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Senha { get; set; }
        public IEnumerable<Pedido> Pedidos { get; set; }
    }
}