using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace bluemodas_api.Models
{
    public class Pedido
    {
        public Pedido() {}

        public Pedido(int id, int registroId, string observacao)
        {
            this.Id = id;
            this.RegistroId = registroId;
            this.Observacao = observacao;
        }

        public int Id { get; set; }
        public int? RegistroId { get; set; }
        public string Observacao { get; set; }
        public IEnumerable<PedidoProduto> PedidosProdutos { get; set; }
    }
}