using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace bluemodas_api.Models
{
    public class Carrinho
    {
        public Carrinho() {}

        public Carrinho(int id, bool finalizado = false)
        {
            this.Id = id;
            this.Finalizado = finalizado;
        }

        public int Id { get; set; }
        public bool Finalizado { get; set; }
        public IEnumerable<PedidoProduto> PedidosProdutos { get; set; }
        
    }
}